package ru.inshakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.service.IAuthService;
import ru.inshakov.tm.api.service.IUserService;
import ru.inshakov.tm.enumerated.Role;
import ru.inshakov.tm.exception.empty.EmptyLoginException;
import ru.inshakov.tm.exception.empty.EmptyPasswordException;
import ru.inshakov.tm.exception.entity.UserNotFoundException;
import ru.inshakov.tm.exception.user.AccessDeniedException;
import ru.inshakov.tm.model.User;
import ru.inshakov.tm.util.HashUtil;

import java.util.Optional;

public final class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @Nullable
    private String userId;

    public AuthService(@NotNull final IUserService userService) {
        this.userService = userService;
    }

    public void setCurrentUserId(User user) {
        userId = user.getId();
    }

    @NotNull
    @Override
    public String getUserId() {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        return userId;
    }

    @Override
    public void checkRoles(@NotNull final Role... roles) {
        if (!Optional.ofNullable(roles).filter(item -> item.length != 0).isPresent()) return;
        @Nullable final User user = getUser();
        Optional.ofNullable(user).orElseThrow(AccessDeniedException::new);
        @Nullable final Role role = user.getRole();
        Optional.ofNullable(role).orElseThrow(AccessDeniedException::new);
        for (@NotNull final Role item: roles) {
            if (item.equals(role)) return;
        }
        throw new AccessDeniedException();
    }

    @Nullable
    @Override
    public User getUser() {
        @NotNull final String userId = getUserId();
        return userService.findById(userId);
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

    @Override
    public void logout() {
        userId = null;
    }

    public void login(@Nullable final String login, @Nullable final String password) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).orElseThrow(EmptyPasswordException::new);
        @Nullable final User user = userService.findByLogin(login);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        setCurrentUserId(user);
        @NotNull final String hash = HashUtil.salt(password);
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        System.out.println("[LOGGED IN]");
    }

    public void registry(@Nullable final String login, final @Nullable String password, final @Nullable String email) {
        userService.create(login, password, email);
    }

}

