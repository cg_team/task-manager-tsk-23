package ru.inshakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.repository.IBusinessRepository;
import ru.inshakov.tm.api.service.IBusinessService;
import ru.inshakov.tm.enumerated.Status;
import ru.inshakov.tm.exception.empty.EmptyIdException;
import ru.inshakov.tm.exception.empty.EmptyNameException;
import ru.inshakov.tm.exception.empty.EmptyStatusException;
import ru.inshakov.tm.exception.entity.EntityNotFoundException;
import ru.inshakov.tm.exception.system.IndexIncorrectException;
import ru.inshakov.tm.exception.user.AccessDeniedException;
import ru.inshakov.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public class AbstractBusinessService<E extends AbstractBusinessEntity>
        extends AbstractService<E> implements IBusinessService<E> {

    @NotNull
    IBusinessRepository<E> entityRepository;

    public AbstractBusinessService(@NotNull IBusinessRepository<E> entityRepository) {
        super(entityRepository);
        this.entityRepository = entityRepository;
    }

    @NotNull
    @Override
    public E changeStatusById(@NotNull final String userId, @Nullable final String id, @Nullable final Status status) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        @Nullable final E entity = findOneById(userId, id);
        Optional.ofNullable(entity).orElseThrow(EntityNotFoundException::new);
        entity.setStatus(status);
        if (status == Status.IN_PROGRESS) entity.setDateStart(new Date());
        if (status == Status.COMPLETE) entity.setDateFinish(new Date());
        return entity;
    }

    @NotNull
    @Override
    public E  changeStatusByIndex(@NotNull final String userId, @Nullable final Integer index, @Nullable final Status status) {
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        Optional.ofNullable(index).filter(item -> item <0).orElseThrow(IndexIncorrectException::new);
        @Nullable final E entity = findOneByIndex(userId, index);
        Optional.ofNullable(entity).orElseThrow(EntityNotFoundException::new);
        entity.setStatus(status);
        if (status == Status.IN_PROGRESS) entity.setDateStart(new Date());
        if (status == Status.COMPLETE) entity.setDateFinish(new Date());
        return entity;
    }

    @NotNull
    @Override
    public E changeStatusByName(@NotNull final String userId, @Nullable final String name, @Nullable final Status status) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        @Nullable final E entity = findOneByName(userId, name);
        Optional.ofNullable(entity).orElseThrow(EntityNotFoundException::new);
        entity.setStatus(status);
        if (status == Status.IN_PROGRESS) entity.setDateStart(new Date());
        if (status == Status.COMPLETE) entity.setDateFinish(new Date());
        return entity;
    }

    @Override
    public void clear(@NotNull String userId) {
        entityRepository.clear(userId);
    }

    @NotNull
    @Override
    public E finishById(@NotNull final String userId, @Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        @Nullable final E entity = findOneById(userId, id);
        Optional.ofNullable(entity).orElseThrow(EntityNotFoundException::new);
        entity.setStatus(Status.COMPLETE);
        entity.setDateFinish(new Date());
        return entity;
    }

    @NotNull
    @Override
    public E finishByName(@NotNull final String userId, @Nullable final String name) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @Nullable final E entity = findOneByName(userId, name);
        Optional.ofNullable(entity).orElseThrow(EntityNotFoundException::new);
        entity.setStatus(Status.COMPLETE);
        entity.setDateFinish(new Date());
        return entity;
    }

    @NotNull
    @Override
    public E finishByIndex(@NotNull final String userId, @Nullable final Integer index) {
        Optional.ofNullable(index).filter(item -> item <0).orElseThrow(IndexIncorrectException::new);
        @Nullable final E entity = findOneByIndex(userId, index);
        Optional.ofNullable(entity).orElseThrow(EntityNotFoundException::new);
        entity.setStatus(Status.COMPLETE);
        entity.setDateFinish(new Date());
        return entity;
    }

    @Nullable
    @Override
    public List<E> findAll(@NotNull final String userId, @Nullable final Comparator<E> comparator) {
        if (!Optional.ofNullable(comparator).isPresent()) return null;
        return entityRepository.findAll(userId, comparator);
    }

    @Nullable
    @Override
    public List<E> findAll(@NotNull final String userId) {
        return entityRepository.findAll(userId);
    }

    @Nullable
    @Override
    public E findOneById(@NotNull final String userId, @Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        return entityRepository.findOneById(userId, id);
    }

    @Nullable
    @Override
    public E findOneByIndex(@NotNull final String userId, @Nullable final Integer index) {
        Optional.ofNullable(index).filter(item -> item <0).orElseThrow(IndexIncorrectException::new);
        return entityRepository.findOneByIndex(userId, index);
    }

    @Nullable
    @Override
    public E findOneByName(@NotNull final String userId, @Nullable final String name) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        return entityRepository.findOneByName(userId, name);
    }

    @Nullable
    @Override
    public E removeOneById(@NotNull final String userId, @Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        return entityRepository.removeOneById(userId, id);
    }

    @Nullable
    @Override
    public E removeOneByIndex(@NotNull final String userId, @Nullable final Integer index) {
        Optional.ofNullable(index).filter(item -> item <0).orElseThrow(IndexIncorrectException::new);
        return entityRepository.removeOneByIndex(userId, index);
    }

    @Nullable
    @Override
    public E removeOneByName(@NotNull final String userId, @Nullable final String name) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        return entityRepository.removeOneByName(userId, name);
    }

    @NotNull
    @Override
    public E startById(@NotNull final String userId, @Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        @Nullable final E entity = findOneById(userId, id);
        Optional.ofNullable(entity).orElseThrow(EntityNotFoundException::new);
        entity.setStatus(Status.IN_PROGRESS);
        entity.setDateStart(new Date());
        return entity;
    }

    @NotNull
    @Override
    public E startByName(@NotNull final String userId, @Nullable final String name) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @Nullable final E entity = findOneByName(userId, name);
        Optional.ofNullable(entity).orElseThrow(EntityNotFoundException::new);
        entity.setStatus(Status.IN_PROGRESS);
        entity.setDateStart(new Date());
        return entity;
    }

    @NotNull
    @Override
    public E startByIndex(@NotNull final String userId, @Nullable final Integer index) {
        Optional.ofNullable(index).filter(item -> item <0).orElseThrow(IndexIncorrectException::new);
        @Nullable final E entity = findOneByIndex(userId, index);
        Optional.ofNullable(entity).orElseThrow(EntityNotFoundException::new);
        entity.setStatus(Status.IN_PROGRESS);
        entity.setDateStart(new Date());
        return entity;
    }

    @NotNull
    @Override
    public E updateByIndex(@NotNull final String userId, @Nullable final Integer index, @Nullable final String name, @Nullable final String description) {
        Optional.ofNullable(index).filter(item -> item <0).orElseThrow(IndexIncorrectException::new);
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @Nullable final E entity = findOneByIndex(userId, index);
        Optional.ofNullable(entity).orElseThrow(EntityNotFoundException::new);
        entity.setName(name);
        entity.setDescription(description);
        return entity;
    }

    @NotNull
    @Override
    public E updateById(@NotNull final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @Nullable final E entity = findOneById(userId, id);
        Optional.ofNullable(entity).orElseThrow(EntityNotFoundException::new);
        entity.setName(name);
        entity.setDescription(description);
        return entity;
    }

}

