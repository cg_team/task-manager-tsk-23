package ru.inshakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.service.ILoggerService;

import java.io.IOException;
import java.util.logging.*;

public final class LoggerService implements ILoggerService {

    @NotNull
    private static final String COMMANDS = "COMMANDS";
    @NotNull
    private static final String COMMANDS_FILE = "./log/commands.txt";

    @NotNull
    private static final String ERRORS = "ERRORS";
    @NotNull
    private static final String ERRORS_FILE = "./log/errors.txt";

    @NotNull
    private static final String MESSAGES = "MESSAGES";
    @NotNull
    private static final String MESSAGES_FILE = "./log/messages.txt";

    @NotNull
    private final LogManager manager = LogManager.getLogManager();
    @NotNull
    private final Logger root = Logger.getLogger("");
    @NotNull
    private final Logger commands = Logger.getLogger(COMMANDS);
    @NotNull
    private final Logger errors = Logger.getLogger(ERRORS);
    @NotNull
    private final Logger messages = Logger.getLogger(MESSAGES);

    {
        init();
        registry(commands, COMMANDS_FILE, false);
        registry(errors, ERRORS_FILE, true);
        registry(messages, MESSAGES_FILE, true);
    }

    @NotNull
    private ConsoleHandler getConsoleHandler() {
        @NotNull final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return handler;
    }

    private void init() {
        try {
            manager.readConfiguration(LoggerService.class.getResourceAsStream("/logger.properties"));
        } catch (@NotNull final IOException e) {
            root.severe(e.getMessage());
        }
    }

    public void registry(@NotNull final Logger logger, @NotNull final String fileName, final boolean isConsole) {
        try {
            if (isConsole) logger.addHandler(getConsoleHandler());
            logger.setUseParentHandlers(false);
            logger.addHandler(new FileHandler(fileName));
        } catch (@NotNull final IOException e) {
            root.severe(e.getMessage());
        }
    }

    @Override
    public void info(@NotNull final String message) {
        if (message.isEmpty()) return;
        messages.info(message);
    }

    @Override
    public void debug(@NotNull final String message) {
        if (message.isEmpty()) return;
        messages.fine(message);
    }

    @Override
    public void command(@Nullable final String message) {
        if (message != null && message.isEmpty()) return;
        commands.info(message);
    }

    @Override
    public void error(@NotNull final Exception e) {
        errors.log(Level.SEVERE, e.getMessage(), e);
    }

}
