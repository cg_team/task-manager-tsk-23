package ru.inshakov.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.repository.ICommandRepository;
import ru.inshakov.tm.api.repository.IUserRepository;
import ru.inshakov.tm.api.service.*;
import ru.inshakov.tm.api.repository.IProjectRepository;
import ru.inshakov.tm.api.repository.ITaskRepository;
import ru.inshakov.tm.command.AbstractCommand;
import ru.inshakov.tm.command.project.*;
import ru.inshakov.tm.command.system.*;
import ru.inshakov.tm.command.task.*;
import ru.inshakov.tm.command.user.*;
import ru.inshakov.tm.enumerated.Role;
import ru.inshakov.tm.exception.entity.ProjectNotFoundException;
import ru.inshakov.tm.exception.system.UnknownArgumentException;
import ru.inshakov.tm.exception.system.UnknownCommandException;
import ru.inshakov.tm.repository.CommandRepository;
import ru.inshakov.tm.repository.ProjectRepository;
import ru.inshakov.tm.repository.TaskRepository;
import ru.inshakov.tm.repository.UserRepository;
import ru.inshakov.tm.service.*;
import ru.inshakov.tm.util.TerminalUtil;

import java.util.Optional;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    private final IAuthService authService = new AuthService(userService);

    {
        registry(new AboutCommand());
        registry(new ArgumentListCommand());
        registry(new CommandListCommand());
        registry(new ExitCommand());
        registry(new HelpCommand());
        registry(new InfoCommand());
        registry(new VersionCommand());
        registry(new TaskCreateCommand());
        registry(new TaskBindByProjectIdCommand());
        registry(new TasksShowByProjectIdCommand());
        registry(new TaskUnbindByProjectId());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskChangeStatusByNameCommand());
        registry(new TaskClearCommand());
        registry(new TaskCreateCommand());
        registry(new TaskFinishByIdCommand());
        registry(new TaskFinishByIndexCommand());
        registry(new TaskFinishByNameCommand());
        registry(new TaskListCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskRemoveByNameCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByNameCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskStartByNameCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new ProjectCascadeRemoveCommand());
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectChangeStatusByNameCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectFinishByIdCommand());
        registry(new ProjectFinishByIndexCommand());
        registry(new ProjectFinishByNameCommand());
        registry(new ProjectListCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectRemoveByNameCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectShowByNameCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectStartByNameCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new UserChangePasswordCommand());
        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserRegistryCommand());
        registry(new UserUpdateProfileCommand());
        registry(new UserViewProfileCommand());
        registry(new UserLockByLoginCommand());
        registry(new UserUnlockByLoginCommand());
        registry(new UserRemoveByLoginCommand());

    }

    private void initUsers() {
        userService.create("test1", "test1", "test1@test.com");
        userService.create("test2", "test2", "test2@test.com");
        userService.create("test3", "test3", "test3@test.com");
        userService.create("test4", "test4", "test4@test.com");
        userService.create("admin", "admin", Role.ADMIN);
    }


    public void run(@Nullable final String... args) {
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
        if (parseArgs(args)) System.exit(0);
        initUsers();
        while (true) {
            System.out.println("ENTER COMMAND:");
            @Nullable final String command = TerminalUtil.nextLine();
            loggerService.command(command);
            try{
                parseCommand(command);
            } catch (final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    private void registry(@Nullable final AbstractCommand command) {
        if (!Optional.ofNullable(command).isPresent()) return;
        command.setIServiceLocator(this);
        commandService.add(command);
    }

    public void parseArg(@Nullable final String arg) {
        if (!Optional.ofNullable(arg).isPresent()) return;
        @Nullable final AbstractCommand command = commandService.getCommandByArg(arg);
        Optional.ofNullable(command).orElseThrow(() -> new UnknownArgumentException(arg));
        command.execute();
    }

    public boolean parseArgs(@Nullable String[] args) {
        if (!Optional.ofNullable(args).filter(item -> item.length != 0).isPresent()) return false;
        String arg = args[0];
        parseArg(arg);
        return true;
    }

    public void parseCommand(@Nullable final String name) {
        if (!Optional.ofNullable(name).isPresent()) return;
        final AbstractCommand command = commandService.getCommandByName(name);
        Optional.ofNullable(command).orElseThrow(() -> new UnknownCommandException(name));
        @Nullable final Role[] roles = command.roles();
        authService.checkRoles(roles);
        command.execute();
    }

    @NotNull
    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @NotNull
    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @NotNull
    @Override
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public IAuthService getAuthService() {
        return authService;
    }
}
