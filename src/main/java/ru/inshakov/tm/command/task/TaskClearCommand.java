package ru.inshakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.command.AbstractTaskCommand;
import ru.inshakov.tm.exception.entity.ProjectNotFoundException;
import ru.inshakov.tm.exception.user.AccessDeniedException;

import java.util.Optional;

public final class TaskClearCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "clear all tasks";
    }

    @Override
    public void execute() {
        @Nullable final String userId = IServiceLocator.getAuthService().getUserId();
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        System.out.println("[TASK CLEAR]");
        IServiceLocator.getTaskService().clear(userId);
        System.out.println("[OK]");
    }

}