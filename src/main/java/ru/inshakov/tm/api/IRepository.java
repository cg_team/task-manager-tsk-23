package ru.inshakov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.model.AbstractEntity;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    void add(@NotNull E entity);

    void clear();

    @Nullable
    List<E> findAll();

    @Nullable
    E findById(@NotNull String id);

    void remove(@NotNull E entity);

    @Nullable
    E removeById(@NotNull String id);

}
