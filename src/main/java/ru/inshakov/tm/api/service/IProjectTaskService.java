package ru.inshakov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.model.Task;
import ru.inshakov.tm.model.Project;

import java.util.List;

public interface IProjectTaskService {

    @NotNull
    List<Task> findAllTasksByProjectId(@Nullable String userId, @NotNull String projectId);

    @NotNull
    Task bindTaskByProjectId(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    @NotNull
    Task unbindTaskByProjectId(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    @Nullable
    Project removeProjectById(@Nullable String userId, @Nullable String projectId);

}
